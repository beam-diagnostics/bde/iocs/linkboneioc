< envVars

errlogInit(20000)

dbLoadDatabase("$(TOP_DIR)/dbd/$(APP).dbd")
$(APP)_registerRecordDeviceDriver(pdbbase) 

# specify the TCP endpoint and port name
epicsEnvSet("LOCATION",                     "LAB")
epicsEnvSet("DEVICE_IP",                    "172.30.150.76")
epicsEnvSet("DEVICE_PORT",                  "23")
epicsEnvSet("DEVICE_NAME",                  "LB-SW-01")
epicsEnvSet("PREFIX",                       "$(LOCATION):$(DEVICE_NAME):")
epicsEnvSet("PORT",                         "LINKBONE")

# configure StreamDevice path
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(DB_DIR)")

# report streamdevice errors
var streamError 1
# report streamdevice debug messages (LOTS!)
# var streamDebug 1

drvAsynIPPortConfigure("$(PORT)", "$(DEVICE_IP):$(DEVICE_PORT)")
 
# laod database defining the EPICS records
dbLoadRecords(linkbone.template, "P=$(PREFIX),R=,PORT=$(PORT)")

# asynSetTraceIOMask("$(PORT)",0,2)
# asynSetTraceMask("$(PORT)",0,255)

###############################################################################
iocInit
###############################################################################

date
###############################################################################
